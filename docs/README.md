# BearExxa Build

Just some documentation on printing and building the [BearExxa](https://github.com/gregsaun/bear_extruder_and_x_axis) Prusa.

## Parts checklist

### BearExxa Parts

| Part | Quantity | |
|:----|:----:|:----:|
| [cable_guide_back_a](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/cable_guide_back_a.stl)     | 1 | :heavy_check_mark: |
| [cable_guide_back_b](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/cable_guide_back_b.stl)     | 1 | :heavy_check_mark: |
| [extruder_body](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/extruder_body.stl)          | 1 | :heavy_check_mark: |
| [extruder_cover](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/extruder_cover.stl)         | 1 | :heavy_check_mark: |
| [extruder_idler](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/extruder_idler.stl)         | 1 | :heavy_check_mark: |
| [filament_sensor_cover](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/filament_sensor_cover.stl) | 1 | :heavy_check_mark: |
| [filament_sensor_lever](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/filament_sensor_lever.stl)  | 1 | :heavy_check_mark: |
| [hotend_collet_clip](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/hotend_collet_clip.stl)     | 1 | :x: |
| [nozzle_fan_duct](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/nozzle_fan_duct.stl)        | 1 | :heavy_check_mark: |
| [ptfe_cutter_50mm_60deg](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/ptfe_cutter_50mm_60deg.stl) | 1 | :heavy_check_mark: |
| [x_carriage](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/mk3s/x_carriage.stl)             | 1 | :heavy_check_mark: |
| [x_carriage_back](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/mk3s/x_carriage_back.stl)        | 1 | :heavy_check_mark: |
| [x_end_idler](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/x_end_idler.stl)            | 1 | :heavy_check_mark: |
| [x_end_idler_tensioner](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/x_end_idler_tensioner.stl)  | 1 | :heavy_check_mark: |
| [x_end_motor](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/mk3s/x_end_motor.stl)            | 1 | :heavy_check_mark: |
| [lcd-cover-original-mk3](https://www.prusaprinters.org/prints/3726-prusa-lcd-surround-in-multicolor) | 1 | :heavy_check_mark: |
| :heavy_exclamation_mark: [z_motor_mounts_fix_mk3_v1.1](https://www.thingiverse.com/thing:2775169) | 2 | :heavy_check_mark: |
| :heavy_exclamation_mark: [z_motor_mounts_fix_front_v1.1](https://www.thingiverse.com/thing:2775169) | 2 | :heavy_check_mark: |

!> **Use M3x12mm to fix the motor (instead of the original M3x10mm)!**

### Other parts

| Part | Quantity | |
|:----|:----:|:----:|
| [Cable_Clip](https://www.thingiverse.com/thing:3565137) | 6 | :heavy_check_mark: |
| [wyze-mountv2-8mm-wide-channel-and-clip](https://www.prusaprinters.org/prints/32687-slide-in-wyze-cam-prusa-mount) | 1 | :heavy_check_mark: |
| [dual_mount_z-axis_top](https://www.prusaprinters.org/prints/3267-led-light-bar-prusa-i3-mk2mk3) | 2 | :heavy_check_mark: |
| [Oreo_Dial](https://www.thingiverse.com/thing:3337198) | 1 | :heavy_check_mark: |

### Optional parts

| Part | Quantity |
|:----|:----:|
| [OctoPi Case](https://www.thingiverse.com/thing:2334119) | 1 |

## Building

!> **Important**: Follow the guide completely. That means eating the correct amount of gummy bears listed.

Your kit will come with a printed building guide, but I recommend using the [online guide](https://help.prusa3d.com/en/guide/1-introduction_24976). There's a comment section at each part with helpful information.

When you get to [3. X-axis assembly](https://help.prusa3d.com/en/guide/3-x-axis-assembly_27464) you'll need to switch over to the [BearExxa guide](https://guides.bear-lab.com/Guide/1.+Preflight+check+and+disassembly/34?lang=en). Do the same for [5. E-axis assembly](https://help.prusa3d.com/en/guide/5-e-axis-assembly_28536).

?> **Tip**: There is an easy mistake that n00bs make. When working on [2. Y-axis assembly](https://help.prusa3d.com/en/guide/2-y-axis-assembly_25488) really focus on where the extrusions are going. It's easy to mount them on the wrong side, and you end up having to redo it.

## Recommended upgrades

- [Sunon MF40100V1-1000U-G99](https://www.printedsolid.com/products/ldo-sunon-fan-mf40100v1-1000c-g99)
- [LED Light Bar Kit](https://www.etsy.com/listing/679440408/led-light-bar-prusa-i3-mk2mk3-please)
- [Raspberry Pi 3B+](https://smile.amazon.com/CanaKit-Raspberry-Power-Supply-Listed/dp/B07BC6WH7V/ref=sr_1_3?dchild=1&keywords=raspberry+pi+3b%2B&qid=1618945994&sr=8-3)
- [6" Right Angle USB Cable for Pi -> Printer](https://smile.amazon.com/gp/product/B00B5HS7TI/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
- [Capricorn Bowden PTFE Tubing](https://smile.amazon.com/Capricorn-Bowden-Filament-Genuine-Premium/dp/B079P92HN9/ref=sr_1_5?dchild=1&keywords=ptfe+Capricorn&qid=1618755363&sr=8-5)
- [Wyze Cam v2 or Black](https://wyze.com/wyze-cam-black.html)
- [Bondhus 10687 Set of 7 Balldriver Screwdrivers](https://smile.amazon.com/gp/product/B0006O4ADI/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
