# Octoprint Setup

## Setting up Octoprint

Download the latest [Octoprint](https://octoprint.org/download/) and write it to an SD card using [Raspberry Pi Imager](https://www.raspberrypi.org/software/).

Before inserting the SD into the Pi, edit a few files in the root of the card.

First, edit the file octopi.txt and update the following:

```text
-r 1920x1080 -f 25
```

## Printer Profile

| Form Factor | Origin | Heated Bed | Width | Depth | Height | Custom Bounding Box |
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
| Rectangular | Lower left | Yes | 250mm | 210mm | 210mm | X: 0/250, Y: -4/210, Z: 0/210 |
