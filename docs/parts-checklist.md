# Parts checklist

## BearExxa Parts

| Part | Quantity | |
|:----|:----:|:----:|
| [cable_guide_back_a](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/cable_guide_back_a.stl)     | 1 | :heavy_check_mark: |
| [cable_guide_back_b](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/cable_guide_back_b.stl)     | 1 | :heavy_check_mark: |
| [extruder_body](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/extruder_body.stl)          | 1 | :heavy_check_mark: |
| [extruder_cover](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/extruder_cover.stl)         | 1 | :heavy_check_mark: |
| [extruder_idler](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/extruder_idler.stl)         | 1 | :heavy_check_mark: |
| [filament_sensor_cover](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/filament_sensor_cover.stl) | 1 | :heavy_check_mark: |
| [filament_sensor_lever](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/filament_sensor_lever.stl)  | 1 | :heavy_check_mark: |
| [hotend_collet_clip](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/hotend_collet_clip.stl)     | 1 | :x: |
| [nozzle_fan_duct](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/nozzle_fan_duct.stl)        | 1 | :x: |
| [ptfe_cutter_50mm_60deg](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/ptfe_cutter_50mm_60deg.stl) | 1 | :heavy_check_mark: |
| [x_carriage](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/mk3s/x_carriage.stl)             | 1 | :heavy_check_mark: |
| [x_carriage_back](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/mk3s/x_carriage_back.stl)        | 1 | :heavy_check_mark: |
| [x_end_idler](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/x_end_idler.stl)            | 1 | :heavy_check_mark: |
| [x_end_idler_tensioner](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/common_to_all_versions/x_end_idler_tensioner.stl)  | 1 | :heavy_check_mark: |
| [x_end_motor](https://github.com/gregsaun/bear_extruder_and_x_axis/blob/master/printed_parts/stl/mk3s/x_end_motor.stl)            | 1 | :heavy_check_mark: |
| [lcd-cover-original-mk3](https://www.prusaprinters.org/prints/3726-prusa-lcd-surround-in-multicolor) | 1 | :heavy_check_mark: |
| [z_motor_mounts_fix_mk3_v1.1](https://www.thingiverse.com/thing:2775169) | 2 | :heavy_check_mark: |
| [z_motor_mounts_fix_front_v1.1](https://www.thingiverse.com/thing:2775169) | 2 | :heavy_check_mark: |
| [Cable_Clip](https://www.thingiverse.com/thing:3565137) | 6 | :heavy_check_mark: |
| [wyze-mountv2-8mm-wide-channel-and-clip](https://www.prusaprinters.org/prints/32687-slide-in-wyze-cam-prusa-mount) | 1 | :heavy_check_mark: |
| [dual_mount_z-axis_top](https://www.prusaprinters.org/prints/3267-led-light-bar-prusa-i3-mk2mk3) | 2 | :heavy_check_mark: |
| [Oreo_Dial](https://www.thingiverse.com/thing:3337198) | 1 | :heavy_check_mark: |
